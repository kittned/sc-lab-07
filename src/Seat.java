
public class Seat {
	
	private double [][] arrSeatAvailable;
	
	public Seat()
	{
		arrSeatAvailable = DataSeatPrice.ticketPrices;
	}
	
	public boolean chkAvailable(int i, int j)
	{		
		if(arrSeatAvailable[i][j] > 0)
			return true;
		else
			return false;
	}
	public String getSeatName(int row, int col)
	{
		String str="";
		str = (char)(65+row-1)+ (""+col);		
		return str;
	}
	
	public double[][] getSeatAvailable()
	{
		return arrSeatAvailable;
	}
	
	public int getRow()
	{
		return DataSeatPrice.ROW;		
	}
	
	public int getCol()
	{
		return DataSeatPrice.COL;
	}
}
